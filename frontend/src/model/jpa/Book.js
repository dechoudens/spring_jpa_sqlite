import React, { useEffect, useState } from 'react';

const Book = ({object}) => {

  const [data, setData] = useState({})

  useEffect(() => {
    setData(object)
  }, [object]);

  return (
    <li>
      {data.id} : {data.description} ({data.price}.-)
    </li>
  )
}

export default Book