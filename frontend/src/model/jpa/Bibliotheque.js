import React, { useEffect, useState } from 'react';
import Game from './Game';
import Book from './Book';

const Bibliotheque = ({object}) => {

  const [data, setData] = useState({})

  useEffect(() => {
    setData(object)
  }, [object]);

  return (
    <li>
      {data.id} : {data.name}

      <p>Games :</p>
      <ul>
       { data.games?.map(game => <Game key={game.id} object={game} />) }
      </ul>

      <p>Books :</p>
      <ul>
        { data.books?.map(book => <Book key={book.id} object={book} />) }
      </ul>
    </li>
  )
}

export default Bibliotheque