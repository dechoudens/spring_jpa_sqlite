import React, { useEffect, useState } from 'react';

const Game = ({object}) => {

  const [data, setData] = useState({})

  useEffect(() => {
    setData(object)
  }, [object]);

  return (
    <li>
      {data.id} : {data.name} - {data.description}
    </li>
  )
}

export default Game