import React, { useEffect, useState } from 'react';

const ExternalBook = ({object}) => {

    const [data, setData] = useState({})
  
    useEffect(() => {
        console.log("bouya", object)
        setData(object)
    }, [object]);
  
    return (
      <div>
        { data.books?.map(book => <p>{book.title} {book.contributor}: {book.description}</p>) }
      </div>
    )
  }
  
  export default ExternalBook