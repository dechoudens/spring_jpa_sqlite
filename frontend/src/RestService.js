import axios from 'axios';

const BASE_URL = ""
const _Axios = axios.create({
    headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
    },
    baseURL: BASE_URL,
    withCredentials: true,
    maxRedirects: 0,
    validateStatus: status => true,
    timeout: 15000,
})

class RestService{

    list(context){
        return _Axios.get(context)
    }

    create(context, object){
        return _Axios.post(context, object)
    }

    get(context, id){
        return _Axios.get(context + "/" + id)
    }

    update(context, object, id){
        return _Axios.update(context + "/" + id, object)
    }

    delete(context, id){
        return _Axios.delete(context + "/" + id)
    }

    external(object){
        return _Axios.post("external", object)
    }
}

export default new RestService()