import React, { useEffect, useState } from 'react';
import RestService from './RestService';
import Bibliotheque from './model/jpa/Bibliotheque';
import ExternalBook from './model/ExternalBook';

const App = () => {
  const [listBiblio, setListBilbio] = useState([])
  const [externalBooks, setExternalBooks] = useState([])
  const externalBookRequest = {
    method:"GET",
    url:"https://api.nytimes.com/svc/books/v3/lists/current/manga.json",
    params:{
      "api-key":"Wi3urZKhXn7gSq1RUxDYhAi0wAgGyWVf",
    }
  }

  useEffect(() => {
    RestService.list("bibliotheques").then(res => {
      setListBilbio(res.data)
    })
    
    RestService.external(externalBookRequest).then(res => {
      setExternalBooks(res.data)
    }) 
  }, []);

  useEffect(() => {
    console.log("listBiblio", listBiblio)
    console.log("externalBooks", externalBooks)

  }, [listBiblio, externalBooks])



  return (
    <div>
      <div id="GET">
        <ul>
          { listBiblio?.map(biblio => <Bibliotheque key={biblio.id} object={biblio} />) }
        </ul>
      </div>
      <div id="POST"> 
        { externalBooks?.results?.books?.map(book => <p key={book.rank}>{book.title} {book.contributor}: {book.description}</p>) }
      </div>
    </div>
  );
};

export default App;