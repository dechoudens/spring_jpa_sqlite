# Project

The goal here is to learn how to use Spring WebService with JPA and SQLite.

And also to have a portable REST endpoint with ready to use database persistence.


## Adding code
Adding a persistant object with a crud endpoint is simplified as much as possible.

Create :
- The Entity
- The repository
- The service
- The endpoint

It basically amounts to copy-pasting the existing ones for your new object and adapting the names.

# To use

- Have Java 17
- Have Maven
- Run those two commands in a terminal at the root of the project
```cmd
mvn clean install

mvn spring-boot:run
```

- Then you can do http requests on the localhost:8080 url. Example : localhost:8080/books