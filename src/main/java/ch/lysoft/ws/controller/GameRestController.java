package ch.lysoft.ws.controller;

import ch.lysoft.entity.Game;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class GameRestController extends CrudRestAPI<Game> {
    private static final String context = "/games";

    @Override
    @RequestMapping(value = context)
    public ResponseEntity<Object> list() {
        return super.list();
    }

    @Override
    @RequestMapping(value = context + "/{id}")
    public ResponseEntity<Object> get(@PathVariable("id")String id) {
        return super.get(id);
    }

    @Override
    @RequestMapping(value = context, method = RequestMethod.POST)
    public ResponseEntity<Object> create(@RequestBody Game obj) {
        return super.create(obj);
    }

    @Override
    @RequestMapping(value = context + "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> update(@PathVariable("id") String id, @RequestBody Game obj) {
        return super.update(id, obj);
    }

    @Override
    @RequestMapping(value = context + "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> delete(@PathVariable("id") String id) {
        return super.delete(id);
    }
}
