package ch.lysoft.ws.controller;

import ch.lysoft.entity.Book;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class BookRestController extends CrudRestAPI<Book>{
    private static final String context = "/books";

    @Override
    @RequestMapping(value = context)
    public ResponseEntity<Object> list() {
        return super.list();
    }

    @Override
    @RequestMapping(value = context + "/{id}")
    public ResponseEntity<Object> get(@PathVariable("id") String id) {
        return super.get(id);
    }

    @Override
    @RequestMapping(value = context, method = RequestMethod.POST)
    public ResponseEntity<Object> create(@RequestBody Book book) {
        return super.create(book);
    }

    @Override
    @RequestMapping(value = context + "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> update(@PathVariable("id") String id, @RequestBody Book book) {
        return super.update(id, book);
    }

    @Override
    @RequestMapping(value = context + "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> delete(@PathVariable("id") String id) {
        return super.delete(id);
    }
}
