package ch.lysoft.ws.controller;

import ch.lysoft.App;
import ch.lysoft.service.CrudService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

public abstract class CrudRestAPI<T> {
    private static final Logger log = LoggerFactory.getLogger(CrudRestAPI.class);

    @Autowired
    protected CrudService<T> service;

    public ResponseEntity<Object> list() {
        List<T> list = service.list();
        String message = "Retrieving list : \n" + list;
        log.info(message);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    public ResponseEntity<Object> get(String id) {
        T obj = service.get(id);
        String message = "Retrieving object : \n" + obj;
        log.info(message);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    public ResponseEntity<Object> create(T obj) {
        service.create(obj);
        String message = "Object is created successfully : \n" + obj;
        log.info(message);
        return new ResponseEntity<>(obj, HttpStatus.CREATED);
    }

    public ResponseEntity<Object> update(String id, T obj) {
        if (service.update(id, obj)) {
            String message = "Object is updated successfully : \n" + obj;
            log.info(message);
            return new ResponseEntity<>(obj, HttpStatus.OK);
        } else {
            String message = "Object " + id + " not found";
            log.info(message);
            return new ResponseEntity<>(message, HttpStatus.OK);
        }
    }

    public ResponseEntity<Object> delete(String id) {
        if (service.delete(id)) {
            String message = "Object " + id + " is deleted successsfully";
            log.info(message);
            return new ResponseEntity<>(message, HttpStatus.OK);
        } else {
            String message = "Object " + id + " not found";
            log.info(message);
            return new ResponseEntity<>(message, HttpStatus.OK);
        }
    }
}
