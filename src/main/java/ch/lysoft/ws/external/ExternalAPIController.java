package ch.lysoft.ws.external;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;
import java.util.Set;

@RestController
public class ExternalAPIController {
    private static final String context = "/external";

    @RequestMapping(value = context, method = RequestMethod.POST)
    public ResponseEntity<Object> external(@RequestBody RequestData requestData) {
        ResponseEntity<Object> responseEntity = null;
        try {
            HttpRequest.Builder requestBuilder = HttpRequest.newBuilder()
                    .uri(new URI(requestData.getUrl() + getParamsString(requestData.getParams())));
            setHeaders(requestBuilder, requestData.getHeaders());

            switch (requestData.getMethod()) {
                case "GET" -> requestBuilder.GET();
                case "POST" -> requestBuilder.POST(HttpRequest.BodyPublishers.ofString(requestData.getBody()));
            }
            HttpRequest request = requestBuilder.build();
            HttpClient client = HttpClient.newHttpClient();
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

            responseEntity = new ResponseEntity<>(response.body(), HttpStatus.resolve(response.statusCode()));
        } catch (URISyntaxException | InterruptedException | IOException e) {
            e.printStackTrace();
            responseEntity = new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return responseEntity;
    }

    private void setHeaders(HttpRequest.Builder requestBuilder, Map<String, String> headers) {
        if(headers != null){
            Set<String> keys = headers.keySet();
            for(String key : keys){
                requestBuilder.headers(key, headers.get(key));
            }
        }
    }

    public String getParamsString(Map<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        if(params != null){
            result.append("?");
            for (Map.Entry<String, String> entry : params.entrySet()) {
                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
                result.append("&");
            }
        }
        String resultString = result.toString();

        return resultString.length() > 0
                ? resultString.substring(0, resultString.length() - 1)
                : resultString;
    }
}
