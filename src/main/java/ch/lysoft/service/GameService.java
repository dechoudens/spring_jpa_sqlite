package ch.lysoft.service;

import ch.lysoft.entity.Game;
import org.springframework.stereotype.Service;

@Service
public class GameService extends CrudService<Game>{
    @Override
    protected Game replaceValues(Game obj, Game newObj) {
        obj.setDescription(newObj.getDescription());
        obj.setName(newObj.getName());
        return obj;
    }
}
