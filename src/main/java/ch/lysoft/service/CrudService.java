package ch.lysoft.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public abstract class CrudService<T> {

    @Lazy
    @Autowired
    JpaRepository<T, Integer> repository;

    public List<T> list() {
        return repository.findAll();
    }

    public T get(String id) {
        Optional<T> obj = repository.findById(Integer.parseInt(id));
        if(obj.isPresent()){
            return obj.get();
        }

        return null;
    }

    public T create(T obj){
        return repository.save(obj);
    }

    public boolean delete(String id) {
        Optional<T> obj = repository.findById(Integer.parseInt(id));
        if(obj.isPresent()){
            repository.delete(obj.get());
            return true;
        }

        return false;
    }

    public boolean update(String id, T newObj){
        Optional<T> currentObj = repository.findById(Integer.parseInt(id));

        if(currentObj.isPresent()){
            T obj = replaceValues(currentObj.get(), newObj);
            repository.save(obj);
            return true;
        }

        return false;
    }

    protected abstract T replaceValues(T obj, T newObj);
}
