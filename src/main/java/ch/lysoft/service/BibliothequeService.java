package ch.lysoft.service;

import ch.lysoft.entity.Bibliotheque;
import org.springframework.stereotype.Service;

@Service
public class BibliothequeService extends CrudService<Bibliotheque> {

    @Override
    protected Bibliotheque replaceValues(Bibliotheque obj, Bibliotheque newObj) {
        obj.setGames(newObj.getGames());
        obj.setName(newObj.getName());
        return obj;
    }
}
