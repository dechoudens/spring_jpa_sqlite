package ch.lysoft.service;

import ch.lysoft.entity.Book;
import org.springframework.stereotype.Service;

@Service
public class BookService extends CrudService<Book> {
    @Override
    protected Book replaceValues(Book obj, Book newObj){
        obj.setPrice(newObj.getPrice());
        obj.setDescription(newObj.getDescription());
        return obj;
    }
}
