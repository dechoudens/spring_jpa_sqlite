package ch.lysoft.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
public class Book extends ObjectEntity{
    private Integer price;
    private String description;

    @ManyToOne
    @JoinColumn(name = "bibliotheque_id")
    @JsonBackReference
    private Bibliotheque bibliotheque;

    public Bibliotheque getBibliotheque() {
        return bibliotheque;
    }

    public void setBibliotheque(Bibliotheque bibliotheque) {
        this.bibliotheque = bibliotheque;
    }

    public Book() {
    }

    public Book(Integer price, String description) {
        this.price = price;
        this.description = description;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Book{" +
                ", price=" + price +
                ", description='" + description + '\'' +
                '}';
    }
}
