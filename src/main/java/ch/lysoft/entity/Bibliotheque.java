package ch.lysoft.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
public class Bibliotheque extends ObjectEntity{

    private String name;

    @OneToMany(mappedBy="bibliotheque")
    @JsonManagedReference
    private List<Game> games;

    @OneToMany(mappedBy="bibliotheque")
    @JsonManagedReference
    private List<Book> books;

    public String getName() {
        return name;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }

    @Override
    public String toString() {
        return "Bibliotheque{" +
                "name='" + name + '\'' +
                ", games=" + games +
                '}';
    }
}